import argparse
import time
from os.path import join
from pathlib import Path

import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
from os import mkdir

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import check_img_size, check_requirements, check_imshow, non_max_suppression, \
    scale_coords, set_logging, increment_path
from utils.plots import plot_one_box
from utils.torch_utils import select_device, time_synchronized
from utils.custom import create_subimages, plot_boxes_group, load_park_labels


def detect(save_img=False):
    source, weights, view_img, imgsz, split_images, h_label, plot_groups, parking, colors_f, scale_sizes = opt.source, opt.weights, opt.view_img, opt.img_size, \
        opt.split_images, opt.no_label, opt.plot_groups, opt.parking, opt.colors, opt.scale_sizes
    webcam = source.isnumeric() or source.endswith('.txt') or source.lower().startswith(
        ('rtsp://', 'rtmp://', 'http://'))

    lst_xyxy = []
    save_dir = Path(increment_path(Path(opt.project) / opt.name, exist_ok=opt.exist_ok))
    save_dir.mkdir(parents=True, exist_ok=True)

    set_logging()
    device = select_device(opt.device)
    half = device.type != 'cpu'

    model = attempt_load(weights, map_location=device)
    stride = int(model.stride.max())
    imgsz = check_img_size(imgsz, s=stride)
    if half:
        model.half()

    vid_path, vid_writer = None, None
    if webcam:
        view_img = check_imshow()
        cudnn.benchmark = True
        dataset = LoadStreams(source, img_size=imgsz, stride=stride)
    else:
        save_img = True
        dataset = LoadImages(source, img_size=imgsz, stride=stride, scale_sizes=scale_sizes)

    names = model.module.names if hasattr(model, 'module') else model.names
    if colors_f:
        colors = [(17, 209, 252),(252, 132, 17), (77, 2, 114), (17, 17, 252)]
    else:
        colors = [[random.randint(0, 255) for _ in range(3)] for _ in names]

    if split_images:
        save_dir_split = join(save_dir, 'split')
        mkdir(save_dir_split)
        for name in names:
            mkdir(join(save_dir_split, name))

    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))  # run once
    t0 = time.time()

    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()
        img /= 255.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()

        for i, det in enumerate(pred):
            if webcam:
                p, s, im0, frame = path[i], '%g: ' % i, im0s[i].copy(), dataset.count
            else:
                p, s, im0, frame = path, '', im0s, getattr(dataset, 'frame', 0)

            im2 = im0.copy()
            p = Path(p)
            save_path = str(save_dir / p.name)
            s += '%gx%g ' % img.shape[2:]
            if len(det):

                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()
                    s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "

                for *xyxy, conf, cls in reversed(det):
                    lst_xyxy.append(xyxy)
                    if save_img or view_img:
                        label = f'{names[int(cls)]} {conf:.0%}'
                        if split_images:
                            create_subimages(im2, xyxy, save_dir_split, names[int(cls)])
                        if plot_groups or parking != 'NIC':
                            continue;
                        else:
                            if not h_label:
                                plot_one_box(xyxy, im0, label=label, color=colors[int(cls)], line_thickness=1)
                            else:
                                plot_one_box(xyxy, im0, label=None, color=colors[int(cls)], line_thickness=2)

                if parking != 'NIC':
                    try:
                        # path1 = str(Path(path).absolute())
                        # abs_path=os.path.join(path1, parking)
                        # print(abs_path)
                        load_park_labels(parking, im0, det)
                    except:
                        print('error while loading parking anotations')

                if plot_groups:
                    plot_boxes_group(lst_xyxy, im0)
                    lst_xyxy = []

            print(f'{s}Done. ({t2 - t1:.3f}s)')

            if view_img:
                cv2.imshow(str(p), im0)
                cv2.waitKey(1)

            if save_img:
                if dataset.mode == 'image':
                    cv2.imwrite(save_path, im0)
                else:
                    if vid_path != save_path:
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()

                        fourcc = 'mp4v'
                        fps = vid_cap.get(cv2.CAP_PROP_FPS)
                        w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                        h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        vid_writer = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*fourcc), fps, (w, h))
                    vid_writer.write(im0)

    if save_img:
        s = ''
        print(f"Results saved to {save_dir}{s}")

    print(f'Done. ({time.time() - t0:.3f}s)')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='weights/NN_YOLOv5x/weights/best.pt', help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='input_images/', help='source')
    parser.add_argument('--img-size', type=int, default=1280, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.75, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.5, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--no-label', action='store_true', help='hide labels')
    parser.add_argument('--split-images', action='store_true', help='split images into folders by class')
    parser.add_argument('--parking', type=str, default='NIC', help='parking mode (aerial vehicles)')
    parser.add_argument('--plot-groups', action='store_true', help='plot detection color based on groups (distance)')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, 1')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--project', default='output/', help='save results to folder')
    parser.add_argument('--name', default='detekce', help='save results to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    parser.add_argument('--colors', action='store_true', help='force 4 colors')
    parser.add_argument('--scale-sizes', action='store_true', help='choose optimal img size if high')

    opt = parser.parse_args()
    print(opt)
    check_requirements()

    with torch.no_grad():
        detect()
