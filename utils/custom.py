from PIL import Image
from os.path import join
import random
from utils.plots import plot_one_box
import string
import cv2

def random_string():
    allowed_chars = string.ascii_letters
    str_size = 12
    return ''.join(random.choice(allowed_chars) for x in range(str_size))


def create_subimages(im0, xyxy, save_dir, name):
    im1 = im0[int(xyxy[1]):int(xyxy[3]),int(xyxy[0]):int(xyxy[2])]
    im = Image.fromarray(im1)
    file_name = random_string() + '.jpg'
    sp = join(join(save_dir, name), file_name)
    im.save(sp)

def plot_boxes_group(boxes_lst, im0):
    boxes = [[box_tenor, group] for group, box_tenor in enumerate(boxes_lst)]
    radius = 30
    circles = []
    colors = []

    for box in boxes:
        y_center = (int(box[0][1]) + int(box[0][3])) / 2
        x_center = (int(box[0][0]) + int(box[0][2])) / 2
        circles.append((int(x_center), int(y_center)))

    for i in range(0, len(circles)):
        for j in range(0, len(circles)):
            if circle_intersect(circles[i][0], circles[i][1], circles[j][0], circles[j][1], radius) == 0:
                for box in boxes:
                    if box[1] == boxes[i][1]:
                        box[1] = boxes[j][1]

    for item in boxes:
        colors.append([random.randint(0, 255) for _ in range(3)])

    for box in boxes:
        plot_one_box(box[0], im0, label=str(box[1]), color=colors[box[1]], line_thickness=2)




def circle_intersect(x1, y1, x2, y2, radius):
    distance = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    radius_sum = (radius + radius) * (radius + radius);
    if (distance == radius_sum):
        return 1
    elif (distance > radius_sum):
        return -1
    else:
        return 0

def load_park_labels(path, im0, det):
    with open(path, 'r') as f:
        parking_labels = f.read().strip().splitlines()

    img_height, img_width = im0.shape[0], im0.shape[1]
    total, taken, free = 0, 0, 0

    for line in parking_labels:
        _, x, y, w, h = map(float, line.split(' '))
        x_s = int((x - w / 2) * img_width)
        x_e = int((x + w / 2) * img_width)
        y_s = int((y - h / 2) * img_height)
        y_e = int((y + h / 2) * img_height)
        total += 1
        item = [x_s, y_s, x_e, y_e]

        plot_one_box(item, im0, label=None, color=(0, 255, 0), line_thickness=1)

        for *xyxy, conf, cls in reversed(det):
            item2 = [int(xyxy[0]), int(xyxy[1]), int(xyxy[2]), int(xyxy[3])]
            iou = bb_intersection_over_union(item, item2)
            if iou > 0.4:
                cv2.line(im0, (int(item[0]), int(item[1])), (int(item[2]), int(item[3])), (0, 0, 255), 1)
                cv2.line(im0, (int(item[2]), int(item[1])), (int(item[0]), int(item[3])), (0, 0, 255), 1)
                plot_one_box(item, im0, label=None, color=(0, 0, 255), line_thickness=1)
                taken += 1

    print('Celkový počet míst: ', total, ', Počet volných míst: ', total-taken, ', Počet zabraných míst: ', taken)



def bb_intersection_over_union(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    iou = interArea / float(boxAArea + boxBArea - interArea)
    return iou






